import datetime
import calendar
import pandas as pd
import plotly.express as px
import yfinance
import sys


def dayOfWeek(date):
    return calendar.day_name[datetime.datetime.strptime(date, '%Y-%m-%d').weekday()]

def getStockData(ticker):
    df = yfinance.Ticker(ticker).history(period="5y")
    df.reset_index(inplace=True)
    df.rename(columns = {'index' : 'Date'})
    return df

def midPrice(data, ind):
    return (data.iloc[ind]['Open']+data.iloc[ind]['Close'])/2

def calcReturn(data, ind, days):
    r = (midPrice(data, ind+days) - midPrice(data, ind))/midPrice(data, ind)
    return r

def populateReturns(stock_df, days):
    returns = []
    for i in range(0, len(stock_df)-days):
        returns.append(calcReturn(stock_df, i, days))
    return returns + [None] * days

def createChart(ticker):
    stock_df = getStockData(ticker)

    returns_df = pd.DataFrame()
    returns_df['Date'] = stock_df['Date']

    mid_price = []
    weekdays = []
    third = []
    for i in range(0, len(returns_df)):
        weekdays.append(dayOfWeek(str(stock_df.iloc[i]['Date'])[:10]))
        mid_price.append(midPrice(stock_df, i)) 
        if int(str(stock_df.iloc[i]['Date'])[8:10]) < 11:
            third.append('Early')
        elif (11 < int(str(stock_df.iloc[i]['Date'])[8:10]) < 21):
            third.append('Middle')
        else:
            third.append('Late')
    returns_df['Weekday'] = weekdays
    returns_df['Price'] = mid_price
    returns_df['Time_of_month'] = third
    #returns_df['Returns_1m'] = populateReturns(stock_df, 21)
    #returns_df['Returns_3m'] = populateReturns(stock_df, 63)
    returns_df['Returns_1y'] = populateReturns(stock_df, 252)

    fig = px.box(returns_df, x = 'Time_of_month', y='Returns_1y', color='Weekday')

    fig.write_image(f"returns{ticker}.png")
    fig.write_html(f"returns{ticker}.html", include_plotlyjs='cdn')

if __name__ == "__main__":
    createChart(sys.argv[1])