# Best Investing Day

Exploring stock returns to see if there is a specific day or time of month that is better to make an investment

## Usage

`python investingday.py <ticker>` will create a png and html graph (with plotly.js for interactive elements such as hoverable values) for the given ticker, for example:

`python investingday.py SPY` will output

![](returnsSPY.png)

which shows the 1 year returns for SPY grouped by time of month and weekday, as well as the html+js version.
